# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/prebuilder_dependencies:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./pythonPackagesToInstallFromGit.txt ./
RUN \
  set -ex;\
  python3 -c "import sys;import lsb_release;i=lsb_release.get_distro_information();print(i);sys.exit('CODENAME' not in i)";\
  ./installPythonPackagesFromGit.sh ;\
  rm -r ~/.cache/pip || true;\
  rm -rf /tmp/*
